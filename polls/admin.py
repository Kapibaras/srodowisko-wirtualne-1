from django.contrib import admin
from .models import Question, Choice

from .models import Question


@admin.register(Question)  # pkt 2
class QuestionAdmin(admin.ModelAdmin):  # pkt 1
    list_display = ('question_text', 'pub_date')  # pkt 3
    list_filter = ('question_text', 'pub_date')  # pkt 4
    search_fields = ('question_text',)  # pkt 5


@admin.register(Choice)
class ChoiceAdmin(admin.ModelAdmin):
    list_display = ('get_question', 'choice_text', 'votes')  # pkt 6
    list_filter = ('question', 'votes')
    search_fields = ('choice_text', 'question__question_text',)
    raw_id_fields = ('question',)  # pkt 7

    @admin.display(description='Question text', )  # pkt 6
    def get_question(self, obj):
        return obj.question.question_text
